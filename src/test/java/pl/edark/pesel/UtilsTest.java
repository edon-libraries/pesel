/*
 * Copyright (c) Adam Edą Radecki adam.radeck.kontakt@gmail.com 2021
 */

package pl.edark.pesel;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import pl.edark.pesel.enums.Gender;
import pl.edark.pesel.exception.BadPeselException;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.edark.pesel.Utils.*;

/**
 * Author: Adam Edą Radecki adam.radeck.kontakt@gmail.com September 2021
 */

class UtilsTest {

    @ParameterizedTest(name = "Utils.validate({0}) should return {1}")
    @CsvSource({
            "54080415618,true",
            "88071639158,true",
            "53031887227,true",
            "98120312336,true",
            "69042426672,true",
            "87092977894,true",
            "52091695278,true",
            "00312086817,true",
            "59010446876,true",
            "96071384428,true",
            "74061492461,true",
            "58081378169,true",
            "02281569924,true",
            "02291239192,true",
            "72081484178,true",
            "05261986856,true",
            "55040845441,true",
            "99082464758,true",
            "64100249888,true",
            "84120748769,true",
            "60100749229,true",
            "59120721672,true",
            "58040336629,true",
            "91030856985,true",
            "77072913143,true",
            "52031251988,true",
            "81051771219,true",
            "46102878561,true",
            "72011277719,true",
            "47123146446,true",
            "47123146440,false",
            "4712314644,false",
            "471231464460,false",
    })
    void validate(String pesel, boolean expected) {
        assertThat(Utils.validate(pesel)).isEqualTo(expected);
    }

    @ParameterizedTest(name = "Utils.getDayOfBirt({0}) should return date : {1}-{2}-{3}")
    @CsvSource({
            "21291918133, 2021, 9, 19",
            "21291911556, 2021, 9, 19",
            "21291993828, 2021, 9, 19",
            "21291926776, 2021, 9, 19",
            "21291927524, 2021, 9, 19",
            "21291928112, 2021, 9, 19",
            "21291936472, 2021, 9, 19",
            "21291923155, 2021, 9, 19",
            "21291939833, 2021, 9, 19",
            "21291912533, 2021, 9, 19",
    })
    void getDayOfBirthTest(String pesel, int year, int month, int day) throws BadPeselException {
        LocalDate dob = getDayOfBirth(pesel);

        assertThat(dob.getYear()).isEqualTo(year);
        assertThat(dob.getMonthValue()).isEqualTo(month);
        assertThat(dob.getDayOfMonth()).isEqualTo(day);
    }

    @ParameterizedTest(name = "Utils.throwIfInvalid({0}) should throw BadPeselException")
    @CsvSource({
            "123",
            "47123146440",
            "4712314644",
            "471231464460",
            "abcdefghijk",
    })
    void throwIfInvalidTest(String pesel) {
        Assertions.assertThrows(BadPeselException.class, () -> throwIfInvalid(pesel));
    }

    @ParameterizedTest(name = "Utils.getGender({0}) should return {1}")
    @CsvSource({
            "67112948572, MALE",
            "81032356538, MALE",
            "92012194194, MALE",
            "02272338416, MALE",
            "67092396271, MALE",
            "86122792678, MALE",
            "54061497635, MALE",
            "51013058591, MALE",
            "02261218253, MALE",
            "80091055697, MALE",
            "02241853267, FEMALE",
            "86122495687, FEMALE",
            "92030215185, FEMALE",
            "94081923169, FEMALE",
            "86110515629, FEMALE",
            "51032038864, FEMALE",
            "48043079744, FEMALE",
            "50080164767, FEMALE",
            "93090671722, FEMALE",
            "48051691181, FEMALE",
    })
    void getGenderTest(String pesel, Gender gender) throws BadPeselException {
        assertThat(getGender(pesel)).isEqualTo(gender);
    }
}