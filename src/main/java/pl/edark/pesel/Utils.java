/*
 * Copyright (c) Adam Edą Radecki adam.radeck.kontakt@gmail.com 2021
 */

package pl.edark.pesel;

import pl.edark.pesel.enums.Gender;
import pl.edark.pesel.exception.BadPeselException;

import java.time.LocalDate;

import static pl.edark.pesel.enums.Gender.*;

/**
 * Author: Adam Edą Radecki adam.radeck.kontakt@gmail.com September 2021
 */

public class Utils {

    public static boolean validate(String pesel) {

        if (!pesel.matches("\\d{11}"))
            return false;
        char[] digits = pesel.toCharArray();
        int sum = 0;
        for (int i = 0; i < digits.length; i++) {
            int d = Character.getNumericValue(digits[i]);
            if (i == 1 || i == 5 || i == 9) d *= 3;
            else if (i == 2 || i == 6) d *= 7;
            else if (i == 3 || i == 7) d *= 9;
            sum += d;
        }
        return sum % 10 == 0;
    }

    public static LocalDate getDayOfBirth(String pesel) throws BadPeselException {
        throwIfInvalid(pesel);

        int year = 1900 + Integer.parseInt(pesel.substring(0, 2));
        int month = Integer.parseInt(pesel.substring(2, 4));
        int day = Integer.parseInt(pesel.substring(4, 6));

        if (month > 80) {
            year -= 100;
            month -= 80;
        } else if (month > 20) {
            int cnt = month / 20;
            year += cnt * 100;
            month -= cnt * 20;
        }
        return LocalDate.now()
                .withDayOfMonth(day)
                .withMonth(month)
                .withYear(year);
    }

    public static Gender getGender(String pesel) throws BadPeselException {
        throwIfInvalid(pesel);

        return Integer.parseInt(pesel.substring(9, 10)) % 2 == 0 ? FEMALE : MALE;
    }

    public static void throwIfInvalid(String pesel) throws BadPeselException {
        if (!validate(pesel)) throw new BadPeselException();
    }
}
