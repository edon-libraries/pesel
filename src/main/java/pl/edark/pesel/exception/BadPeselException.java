/*
 * Copyright (c) Adam Edą Radecki adam.radeck.kontakt@gmail.com 2021
 */

package pl.edark.pesel.exception;

/**
 * Author: Adam Edą Radecki adam.radeck.kontakt@gmail.com September 2021
 */

public class BadPeselException extends Throwable {

    private static final String MESSAGE = "PESEL number is invalid.";

    public BadPeselException() {
        super(MESSAGE);
    }
}
