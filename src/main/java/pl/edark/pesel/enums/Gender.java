package pl.edark.pesel.enums;

/**
 * Author: Adam Edą Radecki adam.radeck.kontakt@gmail.com September 2021
 */

public enum Gender {
    MALE,
    FEMALE
}
